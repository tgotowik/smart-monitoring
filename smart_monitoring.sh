#!/bin/bash
# Configurations
MAILTO="tgotowik@mail.uni-paderborn.de"
output_file="./smartctl_log_$(date +"%m_%d_%Y").txt"
predecessor_file="./smartctl_log_$(date -d "yesterday" +"%m_%d_%Y").txt"
declare -A attributes_to_monitor
attributes_to_monitor["Reallocated_Sector_Ct"]=4
attributes_to_monitor["Uncorrectable_Error_Cnt"]=4

# Enable Sudo
if sudo true
then
    true
else
    echo 'Root privileges required'
    exit 1
fi

# Compare Attributes
compare_attributes() {
    local current_file=$1
    local previous_file=$2

    for attribute in "${!attributes_to_monitor[@]}"
    do
        current_value=$(awk -v attr="$attribute" '$0 ~ attr {print $'"${attributes_to_monitor[$attribute]}"'}' "$current_file")
        previous_value=$(awk -v attr="$attribute" '$0 ~ attr {print $'"${attributes_to_monitor[$attribute]}"'}' "$previous_file")
        if [ "$current_value" != "$previous_value" ]; then
            differences["$attribute"]="$attribute - Current: $current_value | Previous: $previous_value"
        fi
    done
}

if [ -e "$predecessor_file" ]; then
    for drive in /dev/sd[a-z]
    do
        sudo smartctl -H -A $drive >> "$output_file"
        # check overall-health self-assessment test: if "PASSED": do nothing, if "FAILED": mail
        if [[ -e $drive ]]; then
            smart=$(
            sudo smartctl -H -A $drive 2>/dev/null |
            grep '^SMART overall' |
            awk '{ print $6 }'
            )
            if [[-e $smart == "FAILED"]]; then
                mail_result $drive $smart
            fi
        fi
    done

    compare_attributes "$output_file" "$predecessor_file"

else
    echo "No File from yesterday for comparison: $predecessor_file"
    sudo smartctl -H -A /dev/sda > "$output_file"
fi

for attribute in "${!differences[@]}"
do
    echo "Difference in $attribute: ${differences[$attribute]}"
done

mail_result(){
    SUBJECT="$(hostname): smartctl-check"
    FROM="monitoring@$(hostname).cs.uni-paderborn.de"
        for arg in "$@"
    do
        BODY+="$arg\n"
    done
    echo $BODY
    #echo -e "Subject:${SUBJECT}\n${BODY}" | /usr/sbin/sendmail -f "${FROM}" -t "${MAILTO}"
}
#mail_result
rm $output_file