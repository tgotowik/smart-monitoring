import subprocess
import glob
import json
import os
from datetime import date
import smtplib
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart

################### Configurations ###########################
# Directory to write the logs into
log_dir = "./log"
# E-Mail address to send the mails to and from
to_address = "tgotowik@mail.upb.de"
from_address = "root@ceg-125.cs.upb.de"
# Attributes you want to exclude from the monitoring
exclude_attributes = ["Power_On_Hours", "Power_Cycle_Count","Airflow_Temperature_Cel"]
# Drives you want to exclude
# Default are loop, zram devices and partitions
exclude_drives = "loop|part|zram"
# This is the indicator of how close the value can come to the threshold
thresh_warning_limit = 10
##############################################################


# Return a list of all sd[a-z]
def getDrives():
    # Run the lsblk command and capture its output
    result = subprocess.run(['lsblk', '--output', 'NAME,TYPE', '--nodeps'], capture_output=True, text=True)

    # Check if the command was successful
    if result.returncode == 0:
        # Use awk to filter out unwanted lines (loop devices and partitions)
        filtered_output = subprocess.run(['awk', f'!/{exclude_drives}/'], input=result.stdout, capture_output=True, text=True)
        
        # Check if the awk command was successful
        if filtered_output.returncode == 0:
            # Parse the output to get the drive list
            drive_list = []
            lines = filtered_output.stdout.split('\n')[1:]  # Skip the header line
            for line in lines:
                # Split each line into columns
                columns = line.split()
                if columns:
                    drive_list.append(columns[0])

            return drive_list
        else:
            # Handle the case when the awk command fails
            print(f"Error running awk: {filtered_output.stderr}")
            return None
    else:
        # Handle the case when the lsblk command fails
        print(f"Error running lsblk: {result.stderr}")
        return None



# Run the smartctl command with sudo and process data to a dictionary
def getSmartData(drives):
    smart_data = {}
    for drive in drives:
        try:
            command = ["sudo", "smartctl", "-A", "-H", f"/dev/{drive}"]
            result = subprocess.run(command, capture_output=True, text=True, check=True)
            smart_data[drive] = processSmartData(result.stdout)
        except subprocess.CalledProcessError as e:
            print(f"Error occured on getSmartdata: {e}")
    smart_data = excludeData(smart_data)
    return smart_data

# Process the smart data to a dictionary format
def processSmartData(smart_data_text):
    lines = smart_data_text.split('\n')
    smart_data = {}
    capture_data = False

    for line in lines:
        if capture_data and line.strip():
            data = line.split()
            attribute_name = (data[1])
            values = {
                "ID" : data[0],
                "FLAG" : data[2],
                "VALUE": data[3],
                "WORST": data[4],
                "THRESH": data[5],
                "TYPE": data[6],
                "UPDATED": data[7],
                "WHEN_FAILED": data[8],
                "RAW_VALUE": data[9]
            }
            smart_data[attribute_name] = values

        if line.strip().startswith(('ID#', '-')):
            capture_data = True
    return smart_data

# Delete the excluded attributes from smart data
# Return: dict without those attributes
def excludeData(smart_data):
    for drive in smart_data:
        for attributes in exclude_attributes:
            del smart_data[drive][attributes]
    return smart_data

# Pretty Print JSON string
def pprint(data):
    print(json.dumps(data, indent=4))

# Write smart dict as JSON to file
def writeToFile(smart_data):    
    try:
        if not os.path.exists(log_dir):
            os.makedirs(log_dir) 
        
        today = date.today()
        file_name = f"log-{today}.json"
        file_path = os.path.join(log_dir, file_name)
        
        with open(file_path, 'w') as file:
            json.dump(smart_data, file, indent=4)
        
        print(f"File successfully written to: {file_path}")
    except Exception as e:
        print(f"An error occurred on writeToFile: {e}")



# Read latest modified JSON log file
# return as dict
def readFromFile():
    try:
        files_in_folder = os.listdir(log_dir)
    except PermissionError:
        print(f"Permission denied to access directory '{log_dir}'.")
        return None
    except Exception as e:
        print(f"Error occurred on readFromFile: {e}")
        return None
    
    latest_modified_file = None
    latest_modified_time = 0

    for file in files_in_folder:
        file_path = os.path.join(log_dir, file)
        if os.path.isfile(file_path):
            file_modified_time = os.path.getmtime(file_path)
            if file_modified_time > latest_modified_time:
                latest_modified_time = file_modified_time
                latest_modified_file = file_path

    if latest_modified_file:
        with open(latest_modified_file, 'r') as file:
            latest_smart_data = json.load(file)
            return latest_smart_data

    # If no file was found
    return None

# If latest smart data exists, compare attributes for each drive
# Return: dict with attributes that differ for each drive 
def compareSmartData(current_smart_data, latest_smart_data):
    diff_in_data = {}
    if latest_smart_data is not None:
        for drive in current_smart_data:
            if drive in latest_smart_data:
                diff_in_data[drive] = {}
                for attribute in current_data[drive]:
                    if attribute in latest_smart_data[drive]:
                        ###################################### toDo: useful checks ####################################################
                        if current_data[drive][attribute]["RAW_VALUE"] != latest_smart_data[drive][attribute]["RAW_VALUE"]:
                            diff_in_data[drive][attribute] = {}
                            diff_in_data[drive][attribute]["OLD"] = latest_smart_data[drive][attribute]
                            diff_in_data[drive][attribute]["CURRENT"] = current_data[drive][attribute]
                        if int(latest_smart_data[drive][attribute]["THRESH"]) + thresh_warning_limit == int(latest_smart_data[drive][attribute]["VALUE"]):
                            diff_in_data[drive][attribute]["VALUE_CLOSE_TO_THRESH"] = latest_smart_data[drive][attribute]


            else:
                print(f"{drive} not found in latest_smart_data")
            
        pprint(diff_in_data)
        return diff_in_data
    else:
        return None

def sendMail(body):
    try:
        if body is not None:
            subject = str(os.uname()[1]) + ": S.M.A.R.T. Monitoring"
            smtp_server = "localhost"
            smtp_port = 25
            
            message = MIMEMultipart()
            
            message["From"] = from_address
            message["To"] = to_address
            message["Subject"] = subject
            
            message.attach(MIMEText(json.dumps(body, indent=4), "plain"))
            
            with smtplib.SMTP(smtp_server, smtp_port) as server:
                # E-Mail senden
                server.sendmail(from_address, to_address, message.as_string())
        else:
            print(f"No data to send")
    except Exception as e:
        print(f"An error occurred on sendMail: {str(e)}")

################### Testing ###########################
drives = getDrives()
print(drives)
#current_data = getSmartData(getDrives())
#print(current_data)
#latest_data = readFromFile()
#writeToFile(current_data)
#compared_data = compareSmartData(current_data, latest_data)
#sendMail(compared_data)


"""
--> in progres: get smart data via sudo smartctl -A -H --json=c /dev/nvme0n1 | jq
"""