# smart-monitoring
S.M.A.R.T. monitoring script for linux systems. Tested on Debian/Ubuntu

## Description
This script will monitor your S.M.A.R.T. values of all your drives on linux systems and mail you if one disk is predicted to fail.

## Installation
You will need to install smartmontools 

## License
For open source projects, say how it is licensed.

## Project status
Development
